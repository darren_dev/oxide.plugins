﻿using Xunit;

namespace Oxide.Plugins.Test
{
    public class Class1
    {
        [Fact]
        public void ChatTest()
        {
            var playerArgumentString = "/war against perfrekt bounty 50 nobleed showlog";
            var aam = new AdvancedArgumentManagement();
            aam.Add("against", 1, DoWork);
            aam.Add("bounty", 1, DoWork);
            aam.Add("nobleed", DoWork);
            aam.Add("showlog", DoWork);

            aam.Parse(playerArgumentString, new BasePlayer());
        }

        public void DoWork(BasePlayer player, string[] arguments)
        {
            
        }
    }
}
