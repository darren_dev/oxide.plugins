﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;


/*** CREDITS ***
 * 
 * -- Reference Links
 * - <Oxide Permission System> http://oxidemod.org/threads/using-the-oxide-permission-system.24291/
 * - <Oxide Rust Documentation> http://docs.oxidemod.org/rust/
 * 
 * -- Plugins I referenced to aid in development
 * - <Economics Plugin> http://oxidemod.org/plugins/economics.717
 * - <EasyVote  Plugin> http://oxidemod.org/plugins/easyvote.2102/
 * - <Build     Plugin> http://oxidemod.org/plugins/build.715/
 * - <Vanish    Plugin> http://oxidemod.org/plugins/vanish.1420/
 * - <Gather    Plugin> http://oxidemod.org/plugins/gather-manager.675/
 */


namespace Oxide.Plugins
{
    [Info("War", "SemiDemented", "1.0.0")]
    [Description("Allow a player to declare war against another player")]
    public class War : RustPlugin
    {
        #region Fields

        // System fields
        [OnlinePlayers]
        private readonly Hash<BasePlayer, OnlinePlayer> _onlinePlayers = new Hash<BasePlayer, OnlinePlayer>();
        


        // Permission fields
        private Perm _perm;

        // Config fields
        private Configuration _config;

        // Debug fields
        private bool _debug = true;

        // Argument Management fields
        private AdvancedArgumentManagement _aam;

        #endregion

        void Init()
        {
            _perm = new Perm(permission, this);
            _aam = new AdvancedArgumentManagement(Log);

            _aam.Add("help", HelpCommand);
            _aam.Add("a", 1, DeclareWarCommand);
            _aam.Add("accept", AcceptWarCommand);
            _aam.Add("decline", DeclineWarCommand);
        }

        object OnEntityTakeDamage(BaseCombatEntity entity, HitInfo info)
        {
            var player = entity as BasePlayer;
            if (player == null || info.InitiatorPlayer == null)
            {
                return null;
            }

            Log($"{info.InitiatorPlayer.displayName} is trying to damage {player.displayName} ");
            
            if (_onlinePlayers.ContainsKey(player) && _onlinePlayers[player].CanTakeDamageFrom(_onlinePlayers[info.InitiatorPlayer]))
            {
                Log("Allowed!");
                return null;
            }

            Log("Not Allowed!");
            return 0;
        }

        void OnEntityDeath(BaseCombatEntity entity, HitInfo info)
        {
            if (info == null)
            {
                return;
            }

            var player = entity as BasePlayer;
            if (player != null && info.InitiatorPlayer != null)
            {
                Log($"{player.displayName} is trying to damage {info.InitiatorPlayer.displayName}");

                if (_onlinePlayers[player].CanTakeDamageFrom(_onlinePlayers[info.InitiatorPlayer]))
                {
                    _onlinePlayers[player].Killed();
                }
            }
        }

        #region Chat Commands

        [ChatCommand("war")]
        void CommandDeclareWar(BasePlayer player, string command, string[] args)
        {
            if (args == null || args.Length == 0)
            {
                HelpCommand(player, args);
                return;
            }

            Log($"{player.displayName} commanded {command} with the first arg being {args[0]}");
            _aam.Parse(args, player);
        }
        #endregion

        #region Console Commands

        [ConsoleCommand("freebuild.help")]
        private void WarHelp(ConsoleSystem.Arg args)
        {
            args.ReplyWith("Allow FreeBuild globally: freebuild.allow");
            args.ReplyWith("Forbid FreeBuild globally: freebuild.forbid");
            args.ReplyWith("Check if FreeBuild is allowed: freebuild.allow");
        }

        #endregion

        #region Localization

        protected virtual string _lang(string key, string userId)
        {
            return lang.GetMessage(key, this, userId);
        }

        private new void LoadDefaultMessages()
        {
            lang.RegisterMessages(new Dictionary<string, string>
            {
                [PropertyKeys.NoPermission] = "Check your permissions.",
                [PropertyKeys.MultiplePlayers] = "Multiple Players found: {0}",
                [PropertyKeys.PlayerNotFound] = "Player not found: {0}",
                [PropertyKeys.Help] = "/war -a <Player> \n/war accept\n/war decline",
                [PropertyKeys.WarStart] = "",
                [PropertyKeys.WarDeclared] = "{0} has declared war against you!\nAccept the war: /war accept\nDecline the war: /war decline",
                [PropertyKeys.WarRequested] = "You have declared war against {0}!",
                [PropertyKeys.WarUnavailableFromPlayer] = "{0} is unavailable for war.",
                [PropertyKeys.PlayerAcceptedWar] = "You have accepted the war request!\nYou can now damage {0}, and they can damage you!",
                [PropertyKeys.PlayerDeclinedWar] = "You have declined the war request!\nYou will receive no damange from {0}.",
                [PropertyKeys.PlayerAcceptedWarResponse] = "{0} has accepted your war request!\nYou can now damage {0}, and they can damage you!",
                [PropertyKeys.PlayerDeclinedWarResponse] = "{0} has declined your war request!\nYou will be unable to damage them.",
                [PropertyKeys.NoWarRequest] = "You have no pending war requests.",
                [PropertyKeys.InWarWith] = "You are currently in war with: {0}"
            }, this);
        }
        #endregion

        #region War Plugin Actions
        private void HelpCommand(BasePlayer player, string[] arg)
        {
            var origin = _onlinePlayers[player];
            if (origin.InWar)
            {
                Chat(player, PropertyKeys.InWarWith, origin.InWarWith);
            }

            Log($"Player {player.name} just requested help.");
            Chat(player, PropertyKeys.Help);
        }

        private void DeclareWarCommand(BasePlayer player, string[] arg)
        {
            Log($"{player.displayName} declaring war: {arg}");
            if (!_perm.IsWarAllowed(player))
            {
                return;
            }

            BasePlayer targetPlayer = null;
            if (!FindPlayersSingle(arg[0], player, out targetPlayer))
            {
                return;
            }

            Log($"Setting internal params. player: {player.displayName}, target: {targetPlayer.displayName}");
            var origin = _onlinePlayers[player];
            Log("Got Origin");
            var target = _onlinePlayers[targetPlayer];
            Log("Got Target");

            if (target == null)
            {

                Log("Target is not online");
                Chat(player, PropertyKeys.WarUnavailableFromPlayer, targetPlayer.displayName);
                return;
            }

            Log($"Checking if {target.Player.displayName} is available for War");
            if (!target.AvailableForWar)
            {
                Chat(player, PropertyKeys.WarUnavailableFromPlayer, targetPlayer.displayName);
                return;
            }

            

            Log("Declaring War");
            origin.DeclareWarTo(target);


            Chat(targetPlayer, PropertyKeys.WarDeclared, player.displayName);
            Chat(player, PropertyKeys.WarRequested, targetPlayer.displayName);
        }

        private void AcceptWarCommand(BasePlayer player, string[] arg)
        {
            Log($"{player.displayName} is acceptring War");
            var onlinePlayer = _onlinePlayers[player];

            Log("Got online player");
            if (!onlinePlayer.HasWarRequest)
            {
                Log($"{player.displayName} has no War request");
                return;
            }

            Log("Has a War request");
            
            var declaringPlayer = onlinePlayer.AcceptWarRequest()?.Player;

            if (declaringPlayer == null)
            {
                Log($"No declaring player");
                return;
            }

            Log($"Declaring player: {declaringPlayer.displayName}");

            Chat(player, PropertyKeys.PlayerAcceptedWar, declaringPlayer.displayName);
            Chat(declaringPlayer, PropertyKeys.PlayerAcceptedWarResponse, player.displayName);
        }

        private void DeclineWarCommand(BasePlayer player, string[] arg)
        {
            var onlinePlayer = _onlinePlayers[player];
            if (!onlinePlayer.HasWarRequest)
            {
                return;
            }
            var declaringPlayer = onlinePlayer.DeclineWarRequest().Player;
            Chat(player, PropertyKeys.PlayerDeclinedWar, declaringPlayer.displayName);
            Chat(declaringPlayer, PropertyKeys.PlayerDeclinedWarResponse, player.displayName);
        }

        #endregion

        #region Plugin Specific Helpers

        #endregion

        #region Helpers

        /// <summary>
        /// Send a message back to the player - will split into multiple messages on line break
        /// </summary>
        /// <param name="player">Player to send the message to</param>
        /// <param name="messageKey">Message to send (Including line breaks such as as '\n')</param>
        /// <param name="args">Arguments you want to attach to the message. If more than one argument, each will be appended to the corresponding newline message</param>
        public void Chat(BasePlayer player, string messageKey, params string[] args)
        {
            try
            {
                var message = String.Format(_lang(messageKey, player.UserIDString), args);
                var lines = message.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

                foreach (var line in lines)
                {
                    SendReply(player, $"{_config.ChatPrefix} {line}");
                }
            }
            catch (Exception e)
            {
                Log($"Having trouble formatting your message: {e.Message}");
            }
            
        }

        #endregion

        #region FindPlayer

        private bool FindPlayersSingle(string nameOrIdOrIp, BasePlayer player, out BasePlayer targetPlayer)
        {
            Log($"In FindPlayersSingle: {nameOrIdOrIp}");
            var targets = FindPlayers(nameOrIdOrIp);
            targetPlayer = null;
            if (targets.Count <= 0)
            {
                Log("No Players found");
                Chat(player, PropertyKeys.PlayerNotFound, nameOrIdOrIp);
                return false;
            }
            if (targets.Count == 1)
            {
                targetPlayer = targets.First();
                Log($"One Player found: {targetPlayer.displayName}");
                return true;
            }
            var players = String.Join(", ", targets.Select(p => p.displayName).ToArray());
            Log($"Multiple({players}) Players found");

            Chat(player, PropertyKeys.MultiplePlayers, players);
            return false;
        }

        private static HashSet<BasePlayer> FindPlayers(string nameOrIdOrIp)
        {
            var players = new HashSet<BasePlayer>();
            if (String.IsNullOrEmpty(nameOrIdOrIp)) return players;
            foreach (var activePlayer in BasePlayer.activePlayerList)
            {
                if (activePlayer.UserIDString.Equals(nameOrIdOrIp))
                { players.Add(activePlayer); }
                else if (!String.IsNullOrEmpty(activePlayer.displayName) && activePlayer.displayName.Contains(nameOrIdOrIp, CompareOptions.IgnoreCase))
                { players.Add(activePlayer); }
                else if (activePlayer.net?.connection != null && activePlayer.net.connection.ipaddress.Equals(nameOrIdOrIp))
                { players.Add(activePlayer); }
            }
            foreach (var sleepingPlayer in BasePlayer.sleepingPlayerList)
            {
                if (sleepingPlayer.UserIDString.Equals(nameOrIdOrIp))
                { players.Add(sleepingPlayer); }
                else if (!String.IsNullOrEmpty(sleepingPlayer.displayName) && sleepingPlayer.displayName.Contains(nameOrIdOrIp, CompareOptions.IgnoreCase))
                { players.Add(sleepingPlayer); }
            }
            return players;
        }

        #endregion

        #region Core Helpers

        #endregion

        #region Config

        protected override void LoadDefaultConfig() => _config = Configuration.DefaultConfig();

        protected override void SaveConfig() => Config.WriteObject(_config);

        protected override void LoadConfig()
        {
            base.LoadConfig();
            try
            {
                _config = Config.ReadObject<Configuration>();
                if (_config?.ChatPrefix == null)
                {
                    LoadDefaultConfig();
                }
            }
            catch
            {
                PrintWarning($"Could not read oxide/config/{Name}.json, creating new config file");
                LoadDefaultConfig();
            }
            SaveConfig();
        }

        #endregion

        #region Classes
        public class OnlinePlayer
        {
            public BasePlayer Player;

            private bool _inWar;
            private bool _hasWarRequest;

            // We're not doing this YET
            //private HashSet<OnlinePlayer> _requests;
            //private HashSet<OnlinePlayer> _declarations;

            private OnlinePlayer _warAgainst;
            //private OnlinePlayer _playerIDeclaredWarTo;

            public bool InWar => _inWar;
            public bool HasWarRequest => _hasWarRequest;
            public bool AvailableForWar => _warAgainst == null && !HasWarRequest && !InWar;
            public ulong Id => Player.userID;
            public string InWarWith => _warAgainst?.Player.displayName;

            private void StartWar()
            {
                _inWar = true;
            }

            private void EndWar()
            {
                _inWar = false;
                _warAgainst = null;
                _hasWarRequest = false;
            }

            public void DeclareWarTo(OnlinePlayer player)
            {
                //_declarations.Add(player);
                _hasWarRequest = true;
                _warAgainst = player;
                player.ReceiveWarRequest(this);
            }

            private void ReceiveWarRequest(OnlinePlayer onlinePlayer)
            {
                _hasWarRequest = true;
                _warAgainst = onlinePlayer;
            }


            public OnlinePlayer AcceptWarRequest()
            {
                _warAgainst.StartWar();
                StartWar();

                return _warAgainst;
            }

            public OnlinePlayer DeclineWarRequest()
            {
                var declared = _warAgainst;
                _warAgainst.EndWar();
                EndWar();

                return declared;
            }

            public bool CanTakeDamageFrom(OnlinePlayer onlinePlayer)
            {
                return _inWar && _warAgainst?.Id == onlinePlayer.Id;
            }

            public void Killed()
            {
                _warAgainst?.EndWar();
                EndWar();
            }
        }

        public class Configuration
        {
            [JsonProperty(PropertyName = "ChatPrefix")]
            public string ChatPrefix;

            public static Configuration DefaultConfig()
            {
                return new Configuration
                {
                    ChatPrefix = "[War] ",
                };
            }
        }

        public class PropertyKeys
        {
            public static string NoPermission => "NoPermission";
            public static string MultiplePlayers => "MultiplePlayers";
            public static string PlayerNotFound => "PlayerNotFound";
            public static string Help => "Help";
            public static string WarStart => "WarStart";
            public static string WarDeclared => "WarDeclare";
            public static string WarRequested => "WarRequest";
            public static string WarUnavailableFromPlayer => "WarUnavailableFromPlayer";
            public static string PlayerAcceptedWar => "WarPlayerAccepted";
            public static string PlayerDeclinedWar => "WarPlayerDeclined";
            public static string PlayerAcceptedWarResponse => "WarPlayerAcceptedResponse";
            public static string PlayerDeclinedWarResponse => "WarPlayerDeclinedResponse";
            public static string NoWarRequest => "WarNoCurrentRequest";
            public static string InWarWith => "InWarWith";
        }

        public class Perm
        {
            private readonly string _permAllowed = "war.allow";
            private readonly Core.Libraries.Permission _permission;

            public Perm(Core.Libraries.Permission permission, Core.Plugins.Plugin owner)
            {
                _permission = permission;
                _permission.RegisterPermission(_permAllowed, owner);
            }

            public bool IsWarAllowed(BasePlayer player)
            {
                return player.IsAdmin || _permission.UserHasPermission(player.UserIDString, _permAllowed);
            }
        }

        #endregion

        #region Argument Management

        public class AdvancedArgumentManagement
        {
            // Two Dictionaries used to prevent having another model class
            private readonly Dictionary<string, Action<BasePlayer, string[]>> _pluginChatCommands = new Dictionary<string, Action<BasePlayer, string[]>>();
            private readonly Dictionary<string, int> _argumentTemplates;
            private readonly Action<string> _logMethod;

            /// <summary>
            /// Creates a new instance of the Advanced Argument Management engine
            /// </summary>
            public AdvancedArgumentManagement(Action<string> logMethod)
            {
                _logMethod = logMethod;
                _argumentTemplates = new Dictionary<string, int>();
            }

            /// <summary>
            /// Adds a new Argument Template, with the expected number of parameters
            /// </summary>
            /// <param name="argumentName">
            /// Name of the argument template
            /// </param>
            /// <param name="expectedParameters">
            /// The expected number of parameters for the template
            /// </param>
            /// <param name="task">
            /// Action to execute when parsing the arguments
            /// </param>
            public void Add(string argumentName, int expectedParameters, Action<BasePlayer, string[]> task)
            {
                _logMethod($"Adding argument: {argumentName}");
                _argumentTemplates.Add(argumentName, expectedParameters);
                _pluginChatCommands.Add(argumentName, task);
            }

            /// <summary>
            /// Adds a new Argument Template, without any expected parameters
            /// </summary>
            /// <param name="argumentName">
            /// Name of the argument template
            /// </param>
            /// <param name="task">
            /// Action to execute when parsing the arguments
            /// </param>
            public void Add(string argumentName, Action<BasePlayer, string[]> task)
            {
                _logMethod($"Adding argument: {argumentName}");
                _argumentTemplates.Add(argumentName, 0);
                _pluginChatCommands.Add(argumentName, task);
            }

            /// <summary>
            /// Parse the text arguments and excute the associated Actions
            /// </summary>
            /// <param name="playerArgument">
            /// Arguments from the chat
            /// </param>
            /// <param name="player">
            /// The player sending the chat
            /// </param>
            public void Parse(string[] playerArgument, BasePlayer player)
            {
                var expectedParameters = 0;
                var parentArgumentKey = "";
                var parsed = new Dictionary<string, HashSet<string>>();

                foreach (var argument in playerArgument)
                {
                    _logMethod($"Parsing argument: {argument}");
                    if (expectedParameters > 0)
                    {
                        parsed[parentArgumentKey].Add(argument);
                        expectedParameters--;
                        continue;
                    }

                    expectedParameters = 0;

                    if (_argumentTemplates.ContainsKey(argument))
                    {
                        parsed.Add(argument, new HashSet<string>());
                        parentArgumentKey = argument;
                        expectedParameters = _argumentTemplates[argument];
                    }
                }

                Execute(parsed, player);
            }

            private void Execute(Dictionary<string, HashSet<string>> parsed, BasePlayer player)
            {
                foreach (var p in parsed)
                {
                    _pluginChatCommands[p.Key].Invoke(player, p.Value.ToArray());
                }
            }
        }

        #endregion

        #region debug

        void Log(string message)
        {
            if (!_debug)
            {
                return;
            }

            Puts($"[Debug] {message}");
        }
        #endregion
    }
}
