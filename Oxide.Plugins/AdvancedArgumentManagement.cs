﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Oxide.Plugins
{
    public class AdvancedArgumentManagement
    {
        // Two Dictionaries used to prevent having another model class
        private readonly Dictionary<string, Action<BasePlayer, string[]>> _pluginChatCommands = new Dictionary<string, Action<BasePlayer, string[]>>();
        private readonly Dictionary<string, int> _argumentTemplates;

        /// <summary>
        /// Creates a new instance of the Advanced Argument Management engine
        /// </summary>
        public AdvancedArgumentManagement()
        {
            _argumentTemplates = new Dictionary<string, int>();
        }

        /// <summary>
        /// Adds a new Argument Template, with the expected number of parameters
        /// </summary>
        /// <param name="argumentName">
        /// Name of the argument template
        /// </param>
        /// <param name="expectedParameters">
        /// The expected number of parameters for the template
        /// </param>
        /// <param name="task">
        /// Action to execute when parsing the arguments
        /// </param>
        public void Add(string argumentName, int expectedParameters, Action<BasePlayer, string[]> task)
        {
            _argumentTemplates.Add(argumentName, expectedParameters);
            _pluginChatCommands.Add(argumentName, task);
        }

        /// <summary>
        /// Adds a new Argument Template, without any expected parameters
        /// </summary>
        /// <param name="argumentName">
        /// Name of the argument template
        /// </param>
        /// <param name="task">
        /// Action to execute when parsing the arguments
        /// </param>
        public void Add(string argumentName, Action<BasePlayer, string[]> task)
        {
            _argumentTemplates.Add(argumentName, 0);
            _pluginChatCommands.Add(argumentName, task);
        }

        /// <summary>
        /// Parse the text arguments and excute the associated Actions
        /// </summary>
        /// <param name="playerArgument">
        /// Arguments from the chat
        /// </param>
        /// <param name="player">
        /// The player sending the chat
        /// </param>
        public void Parse(string[] playerArgument, BasePlayer player)
        {
            var expectedParameters = 0;
            var parentArgumentKey = "";
            var parsed = new Dictionary<string, HashSet<string>>();

            foreach (var argument in playerArgument)
            {
                if (expectedParameters > 0)
                {
                    parsed[parentArgumentKey].Add(argument);
                    expectedParameters--;
                    continue;
                }

                expectedParameters = 0;

                if (_argumentTemplates.ContainsKey(argument))
                {
                    parsed.Add(argument, new HashSet<string>());
                    parentArgumentKey = argument;
                    expectedParameters = _argumentTemplates[argument];
                }
            }

            Execute(parsed, player);
        }

        private void Execute(Dictionary<string, HashSet<string>> parsed, BasePlayer player)
        {
            foreach (var p in parsed)
            {
                _pluginChatCommands[p.Key].Invoke(player, p.Value.ToArray());
            }
        }
    }
}
