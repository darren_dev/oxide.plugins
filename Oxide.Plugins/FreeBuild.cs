﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;



/*** CREDITS ***
 * 
 * -- Reference Links
 * - <Oxide Permission System> https://oxidemod.org/threads/using-the-oxide-permission-system.24291/
 * - <Oxide Rust Documentation> https://docs.oxidemod.org/rust/
 * 
 * -- Plugins I referenced to aid in development
 * - <Economics Plugin> https://oxidemod.org/plugins/economics.717
 * - <EasyVote  Plugin> https://oxidemod.org/plugins/easyvote.2102/
 * - <Build     Plugin> https://oxidemod.org/plugins/build.715/
 * - <Vanish    Plugin> https://oxidemod.org/plugins/vanish.1420/
 * - <Gather    Plugin> https://oxidemod.org/plugins/gather-manager.675/
 * - <Handyman  Plugin> https://oxidemod.org/plugins/handyman.1737/
 */


namespace Oxide.Plugins
{
    [Info("FreeBuild", "SemiDemented", "1.0.2")]
    [Description("Allow a player to build without resource cost")]
    public class FreeBuild : RustPlugin
    {
        #region Fields

        // System fields
        [OnlinePlayers]
        private readonly Hash<BasePlayer, OnlinePlayer> _onlinePlayers = new Hash<BasePlayer, OnlinePlayer>();

        // Permission fields
        private Perm _perm;

        // Config fields
        private Configuration _config;

        // Debug fields
        private bool _debug = true;
        
        // Argument Management fields
        private AdvancedArgumentManagement _aam;

        #endregion

        void Init()
        {
            _perm = new Perm(permission, this);

            _aam = new AdvancedArgumentManagement(Log);
            _aam.Add("help", HelpCommand);
            _aam.Add("start", StartCommand);
            _aam.Add("stop", StopCommand);
        }

        object CanBuild(Planner planner, Construction prefab, object obj)
        {
            var player = planner.GetOwnerPlayer();
            Log($"Checking if {player.displayName} can build");

            if (_onlinePlayers[player].FreeBuildEnabled)
            {
                Log($"Is deployable: {planner.isTypeDeployable}");

                if (planner.isTypeDeployable)
                {
                    planner.DoPlacement((Construction.Target)obj, prefab);
                }
                else
                {
                    Log("Giving items needed for payment");
                    foreach (var itemAmount in prefab.defaultGrade.costToBuild)
                    {
                        Log($"Giving {itemAmount.itemDef.displayName.english} ({itemAmount.amount})");
                        player.GiveItem(ItemManager.CreateByItemID(itemAmount.itemDef.itemid, (int)itemAmount.amount));
                    }

                    planner.DoBuild((Construction.Target)obj, prefab);
                }
                
                return true;
            }

            Log($"Free Build is disabled for {player.displayName}");

            return null;
        }

        bool CanAffordUpgrade(BasePlayer player, BuildingBlock block, BuildingGrade.Enum grade)
        {
            if (_onlinePlayers[player].FreeBuildEnabled)
            {
                return true;
            }

            if (!CanUpgrade(block, grade))
            {
                return false;
            }

            var costs = GetCost(block, (int)grade);
            return CanAffordUpgrade(costs, player);
        }

        void OnStructureRepair(BaseCombatEntity entity, BasePlayer player)
        {
            if (_onlinePlayers[player].FreeBuildEnabled)
            {
                ((BuildingBlock)entity).SetHealthToMax();
            }
        }

        object OnStructureUpgrade(BaseCombatEntity entity, BasePlayer player, BuildingGrade.Enum grade)
        {
            if (_onlinePlayers[player].FreeBuildEnabled)
            {
                Log("OnStructureUpgrade works!");
                Log($"Grade: {grade.ToString()}");
                ((BuildingBlock)entity).SetHealthToMax();
                ((BuildingBlock)entity).SetGrade(grade);
                ((BuildingBlock)entity).SetHealthToMax();
                ((BuildingBlock)entity).SendNetworkUpdate();
                ((BuildingBlock)entity).UpdateSkin();

                return true;
            }

            return null;
        }


        #region Chat Commands

        [ChatCommand("freebuild")]
        void CommandStartFreeBuild(BasePlayer player, string command, string[] args)
        {
            if (args == null || args.Length == 0)
            {
                HelpCommand(player, args);
                return;
            }
            Log($"{player.displayName} commanded {command} with the first arg being {args[0]}");
            _aam.Parse(args, player);
        }

        #endregion

        #region Console Commands

        [ConsoleCommand("freebuild.help")]
        private void FreeBuildHelp(ConsoleSystem.Arg args)
        {
            args.ReplyWith("Allow FreeBuild globally: freebuild.allow");
            args.ReplyWith("Forbid FreeBuild globally: freebuild.forbid");
            args.ReplyWith("Check if FreeBuild is allowed: freebuild.allow");
        }

        #endregion

        #region Localization

        protected virtual string _lang(string key, string userId, params object[] args)
        {
            return String.Format(lang.GetMessage(key, this, userId), args);
        }

        private new void LoadDefaultMessages()
        {
            lang.RegisterMessages(new Dictionary<string, string>
            {
                [PropertyKeys.Help] = "/freebuild start : Start Free Build mode\n/freebuild stop : Stop Free Build mode",
                [PropertyKeys.FreeBuildStart] = "Free Build started.",
                [PropertyKeys.FreeBuildEnd] = "Free Build stopped.",
                [PropertyKeys.NoPermission] = "Check your permissions.",
                [PropertyKeys.FreeBuildDisallow] = "Free Build has been forbidden via console.",
                [PropertyKeys.MultiplePlayers] = "Multiple Players found: ",
                [PropertyKeys.PlayerNotFound] = "Player not found."
            }, this);
        }
        #endregion

        #region FreeBuild Plugin Actions

        private void HelpCommand(BasePlayer player, string[] arg)
        {
            Log($"Player {player.name} just requested help.");
            Chat(player, PropertyKeys.Help);
        }

        private void StartCommand(BasePlayer player, string[] arg)
        {
            Log($"{player.displayName} has requested to start Free Build");
            
            if (!_perm.IsFreeBuildAllowed(player))
            {
                Chat(player, PropertyKeys.NoPermission);
                return;
            }
            
            Log($"Giving {player.displayName} Free Build");
            _onlinePlayers[player].StartFreeBuild();
            Chat(player, PropertyKeys.FreeBuildStart);
        }

        private void StopCommand(BasePlayer player, string[] arg)
        {
            if (!_perm.IsFreeBuildAllowed(player))
            {
                Chat(player, PropertyKeys.NoPermission);
                return;
            }

            _onlinePlayers[player].StopFreeBuild();
            Chat(player, PropertyKeys.FreeBuildEnd);
        }

        #endregion

        #region Plugin Specific Helpers

        #endregion

        #region Helpers
        
        /// <summary>
        /// Send a message back to the player - will split into multiple messages on line break
        /// </summary>
        /// <param name="player">Player to send the message to</param>
        /// <param name="messageKey">Message to send (Including line breaks such as as '\n')</param>
        /// <param name="args">Arguments you want to attach to the message. If more than one argument, each will be appended to the corresponding newline message</param>
        public void Chat(BasePlayer player, string messageKey, params object[] args)
        {
            var message = String.Format(_lang(messageKey, player.UserIDString), args);
            var lines = message.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

            for (var i = 0; i < lines.Length; i++)
            {
                var line = lines[i];
                if (args.Length >= i + 1)
                {
                    line = $"{line} {args[i]}";
                }
                SendReply(player, $"{_config.ChatPrefix} {line}");
            }
        }

        #endregion
        
        #region Core Helpers

        private bool CanAffordUpgrade(Dictionary<int, float> costs, BasePlayer player)
        {
            foreach (var current in costs)
            {
                var amount = player.inventory.GetAmount(current.Key);
                if (amount >= current.Value)
                {
                    continue;
                }
                return false;
            }
            return true;
        }

        private Dictionary<int, float> GetCost(BuildingBlock block, int targetGrade)
        {
            var grade = NextBlockGrade(block, targetGrade, 1);
            Dictionary<int, float> costs = new Dictionary<int, float>();


            List<ItemAmount> costToBuild = block.blockDefinition.grades[grade].costToBuild;
            foreach (var itemAmount in costToBuild)
            {
                if (!costs.ContainsKey(itemAmount.itemid))
                { costs[itemAmount.itemid] = itemAmount.amount; }
                else
                { costs[itemAmount.itemid] += itemAmount.amount; }
            }

            return costs;
        }

        static bool CanUpgrade(BuildingBlock block, BuildingGrade.Enum grade)
        {
            if (block.IsDestroyed || grade == block.grade)
                return false;
            if ((int)grade > block.blockDefinition.grades.Length)
                return false;
            if (grade < BuildingGrade.Enum.Twigs)
                return false;
            return true;
        }

        static int NextBlockGrade(BuildingBlock building_block, int targetGrade, int offset)
        {
            var grade = (int)building_block.grade;

            var grades = building_block.blockDefinition.grades;
            if (grades == null) return grade;

            if (offset > 0 && targetGrade >= 0 && targetGrade < grades.Length && grades[targetGrade] != null)
                return grade >= targetGrade ? grade : targetGrade;
            if (offset < 0 && targetGrade >= 0 && targetGrade < grades.Length && grades[targetGrade] != null)
                return grade <= targetGrade ? grade : targetGrade;

            targetGrade = grade + offset;
            while (targetGrade >= 0 && targetGrade < grades.Length)
            {
                if (grades[targetGrade] != null) return targetGrade;
                targetGrade += offset;
            }

            return grade;
        }

        #endregion

        #region Config

        protected override void LoadDefaultConfig() => _config = Configuration.DefaultConfig();

        protected override void SaveConfig() => Config.WriteObject(_config);

        protected override void LoadConfig()
        {
            base.LoadConfig();
            try
            {
                _config = Config.ReadObject<Configuration>();
                if (_config?.ChatPrefix == null)
                {
                    LoadDefaultConfig();
                }
            }
            catch
            {
                PrintWarning($"Could not read oxide/config/{Name}.json, creating new config file");
                LoadDefaultConfig();
            }
            SaveConfig();
        }

        #endregion

        #region Classes
        private class OnlinePlayer
        {
            public BasePlayer Player;

            private bool _freeBuildEnabled;

            public bool FreeBuildEnabled => _freeBuildEnabled;

            public void StartFreeBuild()
            {
                _freeBuildEnabled = true;
            }

            public void StopFreeBuild()
            {
                _freeBuildEnabled = false;
            }
        }

        public class Configuration
        {
            [JsonProperty(PropertyName = "ChatPrefix")]
            public string ChatPrefix;

            public static Configuration DefaultConfig()
            {
                return new Configuration
                {
                    ChatPrefix = "[FreeBuild] ",
                };
            }
        }

        public class PropertyKeys
        {
            public static string FreeBuildDisallow => "FreeBuildGlobalDisallow";
            public static string FreeBuildEnd => "FreeBuildEnd";
            public static string FreeBuildStart => "FreeBuildStart";
            public static string NoPermission => "FreeBuildNoPermission";
            public static string MultiplePlayers => "MultiplePlayers";
            public static string PlayerNotFound => "PlayerNotFound";
            public static string Help => "Help";
        }

        public class Perm
        {
            private readonly string _permAllowed = "freebuild.allow";
            private readonly Core.Libraries.Permission _permission;

            public Perm(Core.Libraries.Permission permission, Core.Plugins.Plugin owner)
            {
                _permission = permission;
                _permission.RegisterPermission(_permAllowed, owner);
            }

            public bool IsFreeBuildAllowed(BasePlayer player)
            {
                return player.IsAdmin || _permission.UserHasPermission(player.UserIDString, _permAllowed);
            }
        }

        #endregion

        #region Argument Management

        public class AdvancedArgumentManagement
        {
            // Two Dictionaries used to prevent having another model class
            private readonly Dictionary<string, Action<BasePlayer, string[]>> _pluginChatCommands = new Dictionary<string, Action<BasePlayer, string[]>>();
            private readonly Dictionary<string, int> _argumentTemplates;
            private readonly Action<string> _logMethod;

            /// <summary>
            /// Creates a new instance of the Advanced Argument Management engine
            /// </summary>
            public AdvancedArgumentManagement(Action<string> logMethod)
            {
                _logMethod = logMethod;
                _argumentTemplates = new Dictionary<string, int>();
            }

            /// <summary>
            /// Adds a new Argument Template, with the expected number of parameters
            /// </summary>
            /// <param name="argumentName">
            /// Name of the argument template
            /// </param>
            /// <param name="expectedParameters">
            /// The expected number of parameters for the template
            /// </param>
            /// <param name="task">
            /// Action to execute when parsing the arguments
            /// </param>
            public void Add(string argumentName, int expectedParameters, Action<BasePlayer, string[]> task)
            {
                _logMethod($"Adding argument: {argumentName}");
                _argumentTemplates.Add(argumentName, expectedParameters);
                _pluginChatCommands.Add(argumentName, task);
            }

            /// <summary>
            /// Adds a new Argument Template, without any expected parameters
            /// </summary>
            /// <param name="argumentName">
            /// Name of the argument template
            /// </param>
            /// <param name="task">
            /// Action to execute when parsing the arguments
            /// </param>
            public void Add(string argumentName, Action<BasePlayer, string[]> task)
            {
                _logMethod($"Adding argument: {argumentName}");
                _argumentTemplates.Add(argumentName, 0);
                _pluginChatCommands.Add(argumentName, task);
            }

            /// <summary>
            /// Parse the text arguments and excute the associated Actions
            /// </summary>
            /// <param name="playerArgument">
            /// Arguments from the chat
            /// </param>
            /// <param name="player">
            /// The player sending the chat
            /// </param>
            public void Parse(string[] playerArgument, BasePlayer player)
            {
                var expectedParameters = 0;
                var parentArgumentKey = "";
                var parsed = new Dictionary<string, HashSet<string>>();

                foreach (var argument in playerArgument)
                {
                    _logMethod($"Parsing argument: {argument}");
                    if (expectedParameters > 0)
                    {
                        parsed[parentArgumentKey].Add(argument);
                        expectedParameters--;
                        continue;
                    }

                    expectedParameters = 0;

                    if (_argumentTemplates.ContainsKey(argument))
                    {
                        parsed.Add(argument, new HashSet<string>());
                        parentArgumentKey = argument;
                        expectedParameters = _argumentTemplates[argument];
                    }
                }

                Execute(parsed, player);
            }

            private void Execute(Dictionary<string, HashSet<string>> parsed, BasePlayer player)
            {
                foreach (var p in parsed)
                {
                    _pluginChatCommands[p.Key].Invoke(player, p.Value.ToArray());
                }
            }
        }

        #endregion

        #region debug

        void Log(string message)
        {
            if (!_debug)
            {
                return;
            }

            Puts($"[Debug] {message}");
        }
        #endregion
    }
}
